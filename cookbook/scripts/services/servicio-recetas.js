
angular
    .module("cookbook")
    .service("ServicioRecetas", function($http, Propiedades) {

        // Toda funcionalidad que quieras exponer hacia
        // afuera, tiene que estar publicada en this.
        
        // Obtenemos la colección de recetas.
        this.obtenerRecetas = function() {

            return $http.get(Propiedades.urlServidor + Propiedades.endpointRecetas);
        };

        // Obtenemos la receta correspondiente al identificador indicado.
        this.obtenerReceta = function(recetaId) {

            return $http.get(Propiedades.urlServidor + Propiedades.endpointRecetas + "/" + recetaId);
        };

        // Elimina la receta correspondiente al identificador indicado.
        this.eliminarReceta = function(recetaId) {

            return $http.delete(Propiedades.urlServidor + Propiedades.endpointRecetas + "/" + recetaId);
        };
        
        // Guardamos la receta.
        this.guardarReceta = function(receta, imagen) {

            var promesa;

            // Si la imagen viene dada.
            if (imagen) {

                // Montamos un 'FormData' con la imagen.
                var datos = new FormData();
                datos.append("img", imagen);

                // Configuramos el 'Content-Type' de la petición.
                // Tenemos que indicarlo como 'undefined' para que
                // AngularJS infiera el tipo de la petición.
                var configuracion = {
                    "headers": {
                        "Content-Type": undefined
                    }
                };

                // Subimos la imagen al servidor.
                promesa = $http
                    .post(
                        Propiedades.urlServidor + Propiedades.endpointImagenes,
                        datos,
                        configuracion
                    )
                    .then(function(respuesta) {

                        // En la propiedad 'path' me viene dada
                        // la ruta relativa de la imagen subida.
                        var ruta = respuesta.data.path;

                        // Establecemos la ruta de la imagen en
                        // el objeto receta antes de guardarla.
                        receta.rutaImagen = ruta;

                        return $http.post(Propiedades.urlServidor + Propiedades.endpointRecetas, receta);
                    });
            }

            // En caso de no haber indicado una imagen.
            else {
                promesa = $http.post(Propiedades.urlServidor + Propiedades.endpointRecetas, receta);
            }

            return promesa;
        };

        // Montamos la ruta absoluta a la imagen indicada.
        this.obtenerRutaImagenAbsoluta = function(rutaRelativa) {

            return rutaRelativa
                ? (Propiedades.urlServidor + "/" + rutaRelativa)
                : undefined;
        };
    });