
angular
    .module("cookbook")
    .filter("Ingrediente", function() {

        return function(ingrediente) {
            
            return ingrediente ? (ingrediente.cantidad + " gr. de " + ingrediente.nombre) : "";
        };
    });