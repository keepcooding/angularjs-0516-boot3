
angular
    .module("cookbook")
    .component("raiz", {
        $routeConfig: [{
            name: "MisRecetas",
            path: "/mis-recetas",
            component: "misRecetas"
        }, {
            name: "NuevaReceta",
            path: "/nueva-receta",
            component: "nuevaReceta"
        }, {
            name: "DetalleReceta",
            path: "/receta/:id",
            component: "detalleReceta"
        }],
        templateUrl: "views/raiz.html"
    });