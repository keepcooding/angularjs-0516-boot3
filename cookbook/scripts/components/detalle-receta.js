
angular
    .module("cookbook")
    .component("detalleReceta", {
        bindings: {
            $router: "<"
        },
        templateUrl: "views/detalle-receta.html",
        controller: function(ServicioRecetas) {

            var self = this;

            // El hook '$routerOnActivate' del router nos da información sobre
            // la ruta que se está navegando. Entre otros datos, podemos acceder
            // a los parámetros definidos en la ruta.
            self.$routerOnActivate = function(next) {

                // Recuperamos el identificador de la receta
                // a partir de los parámetros de la ruta.
                var recetaId = next.params.id;

                // Recuperamos la receta correspondiente al identificador recuperado.
                ServicioRecetas.obtenerReceta(recetaId).then(function(respuesta) {
                    self.receta = respuesta.data;
                });
            };

            // Obtenemos la ruta absoluta de la imagen.
            self.obtenerRutaImagen = ServicioRecetas.obtenerRutaImagenAbsoluta;

            // Manejador del botón 'Eliminar receta'.
            self.eliminarReceta = function() {

                // Preguntamos al usuario si está seguro de eliminar la receta.
                if (confirm("¿Estás seguro de eliminar esta receta?")) {

                    // En caso afirmativo, hacemos la petición oportuna al servidor.
                    ServicioRecetas.eliminarReceta(self.receta.id).then(function() {

                        // Una vez eliminada, navegamos a la colección general.
                        self.$router.navigate(["MisRecetas"]);
                    });
                }
            };
        }
    });