
angular
    .module("cookbook")
    .component("misRecetas", {

        // Con 'template' / 'templateUrl' establecemos la vista del componente.
        templateUrl: "views/mis-recetas.html",

        // En 'controller' establecemos la lógica del componente.
        controller: function(ServicioRecetas) {

            var self = this;

            // Filtro para buscar recetas por nombre.
            self.filtroRecetas = { nombre: "" };

            // Podemos engancharnos al hook '$onInit', que se
            // dispara cuando el componente se inicia.
            self.$onInit = function() {

                // Como 'obtenerRecetas()' retorna una promesa, tengo que
                // pasar un manejador a su funcion 'then()'.
                ServicioRecetas.obtenerRecetas().then(function(respuesta) {

                    // En la propiedad 'data' de la respuesta HTTP tenemos el cuerpo de la misma.
                    self.recetas = respuesta.data;
                });
            };

            // Obtenemos la ruta absoluta de la imagen.
            self.obtenerRutaImagen = ServicioRecetas.obtenerRutaImagenAbsoluta;
        }
    });