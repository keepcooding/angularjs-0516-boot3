
// Imports.
import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions } from "@angular/http";
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/map";
import { Receta } from "./receta";

// Cualquier clase decorada con @Injectable es un servicio.
@Injectable()
export class RecetasService {

    // Definimos una variable privada para la URL del servidor. Las variables
    // privadas se suelen definir con un guión bajo precediendo su nombre.
    private _endpoint : string = "http://localhost:9876/api/recetas";

    // Contructor de la clase.
    // Hacemos la inyección de Http.
    constructor(private _http : Http) { }

    // Obtenemos la colección de recetas.
    // Retornamos un array de Observables de tipo Receta.
    obtenerRecetas() : Observable<Receta[]> {

        return this._http
            .get(this._endpoint)
            .map(respuesta => respuesta.json());
    }

    // Guardamos la receta indicada.
    // Retornamos un Observable de tipo Receta.
    guardarReceta(receta : Receta) : Observable<Receta> {

        // Generamos una cadena de texto con el JSON de la receta.
        var datos = JSON.stringify(receta);

        // Establecemos el Content-Type del cuerpo de la petición.
        var cabeceras = new Headers({ "Content-Type": "application/json" });
        var opciones = new RequestOptions({ headers: cabeceras });

        return this._http
            .post(this._endpoint, datos, opciones)
            .map(respuesta => respuesta.json());
    }
}
