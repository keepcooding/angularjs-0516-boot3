
// Imports.
import { Pipe, PipeTransform } from "@angular/core";
import { Ingrediente } from "./receta";

// Toda clase decorada con @Pipe se comporta, precisamente, como un Pipe.
@Pipe({

    // Con el metadato 'name' indicamos el nombre del Pipe a usar en las vistas.
    name: "Ingredientes"
})
// Cuando queramos que una clase implemente una interfaz, lo indicamos con 'implements' en su definición.
export class IngredientesPipe implements PipeTransform {

    // Es obligatorio implementar la interfaz 'PipeTransform' en los Pipes. Al hacerlo, debemos
    // implementar su función 'transform', en la cual indicamos la transformación o el formato
    // que sufrirá el dato de entrada que se indique.
    transform(coleccion: Ingrediente[]) : string {

        coleccion = coleccion || [];

        // Aquí estamos usando una 'arrow function', que es una novedad en ES6, no en TypeScript. Su
        // equivalencia en ES5 sería: coleccion.map(function(ingrediente) { ... });
        var textos = coleccion.map((ingrediente) => {

            // Aquí usamos interpolación en cadenas de texto, que es otra novedad de ES6, al igual
            // que los textos multilínea. Se utiliza el acento grave (`) para definir la cadena de
            // texto, y con ${} indicamos aquellas expresiones y valores a interpolar. Un ejemplo
            // de la siguiente siguiente interpolación sería, por ejemplo, 'Tomate (250 gr.)'.
            return `${ingrediente.nombre} (${ingrediente.cantidad} gr.)`;
        });

        return textos.join(", ");
    }
}