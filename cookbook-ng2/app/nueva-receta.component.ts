
// Imports.
import { Component, OnInit } from "@angular/core";
import { HTTP_PROVIDERS } from "@angular/http";
import { RecetasService } from "./recetas.service";
import { Router } from "@angular/router-deprecated";
import { Receta } from "./receta";
import { Ingrediente } from "./ingrediente";

// Toda clase decorada con @Component se comporta como un componente.
@Component({
    
    // Con el metadato 'template' / 'templateUrls' establecemosla vista asociada al componente.
    template: `
        <div class="row">
            <div class="twelve columns">
                <div>
                    <label for="nombre-receta">Nombre de receta</label>
                    <!-- Con [(ngModel)] hacemos enlaces bidireccionales -->
                    <input id="nombre-receta" type="text" placeholder="Pe. Gazpacho andaluz" class="u-full-width" [(ngModel)]="receta.nombre" />
                </div>
                <div>
                    <label for="ingredientes-receta">Ingredientes</label>
                    <!-- Con [(ngModel)] hacemos enlaces bidireccionales -->
                    <!-- Con () enlazamos manejadores a eventos -->
                    <input id="ingredientes-receta" type="text" placeholder="Pe. Tomate" class="u-full-width" [(ngModel)]="ingrediente.nombre" (keyup)="agregarIngrediente($event)" />
                </div>
                <div>
                    <table class="u-full-width">
                        <thead>
                            <tr>
                                <th>Ingrediente</th>
                                <th colspan="2">Cantidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Con la directiva ngFor replicamos elementos del DOM en base a una colección dada -->
                            <tr *ngFor="let ingrediente of receta.ingredientes">
                                <!-- Con {{ }} hacemos interpolación -->
                                <td>{{ ingrediente.nombre }}</td>
                                <td>
                                    <!-- Con [(ngModel)] hacemos enlaces bidireccionales -->
                                    <input type="number" [(ngModel)]="ingrediente.cantidad">
                                </td>
                                <td>
                                    <!-- Con () enlazamos manejadores a eventos -->
                                    <button (click)="eliminarIngrediente($index)">Eliminar</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <!-- Con () enlazamos manejadores a eventos -->
                    <button class="u-full-width button-primary" (click)="guardarReceta()">Guardar</button>
                </div>
            </div>
        </div>
    `,

    // Con el metadato 'providers' indicamos a Angular cómo tiene que
    // instanciar aquellos servicios que vamos a usar en este componente.
    providers: [
        HTTP_PROVIDERS,
        RecetasService
    ]
})
// Cuando queramos que una clase implemente una interfaz, lo indicamos con 'implements' en su definición.
export class NuevaRecetaComponent implements OnInit {

    // Entidad de tipo Receta.
    receta : Receta;

    // Entidad de tipo Ingrediente.
    ingrediente : Ingrediente;

    // Contructor de la clase.
    // Hacemos la inyección de Router y RecetasService.
    constructor(
        private _router : Router,
        private _recetasService : RecetasService) { }

    // Hook OnInit del componente.
    // Establecemos los valores iniciales de las entidades Receta e Ingrediente.
    ngOnInit() {

        this.receta = {
            nombre: "",
            ingredientes: []
        };

        this.ingrediente = {
            nombre: "",
            cantidad: 1
        };
    }

    // Añadimos el ingrediente indicado en el formulario al pulsar la tecla intro.
    agregarIngrediente(evento) {

        var tecla = evento.which || evento.keyCode;

        if (tecla === 13 && this.ingrediente.nombre) {

            this.receta.ingredientes.push(this.ingrediente);

            this.ingrediente = {
                nombre: "",
                cantidad: 1
            };
        }
    };

    // Eliminamos el ingrediente que está en la posición indicada de la colección.
    eliminarIngrediente(indice : number) {

        this.receta.ingredientes.splice(indice, 1);
    };

    // Guardamos la receta indicada en el formulario y redirigimos
    // el navegador a la colección general de recetas.
    guardarReceta() {

        this._recetasService
            .guardarReceta(this.receta)
            .subscribe(
                receta => this._router.navigate(['MisRecetas']),
                error => alert(error)
            );
    };
}
