
// Imports.
import { Component, OnInit } from "@angular/core";
import { HTTP_PROVIDERS } from "@angular/http";
import { RecetasService } from "./recetas.service";
import { Receta } from "./receta";
import { IngredientesPipe } from "./ingredientes.pipe";

// Toda clase decorada con @Component se comporta como un componente.
@Component({

    // Con el metadato 'template' / 'templateUrls' establecemos la vista asociada al componente.
    template: `
        <div class="row">
            <div class="twelve columns">
                <table class="u-full-width">
                    <tbody>
                        <!-- Con la directiva ngFor replicamos elementos del DOM en base a una colección dada -->
                        <tr *ngFor="let receta of recetas">
                            <td>
                                <!-- Con {{ }} hacemos interpolación -->
                                <div>{{ receta.nombre }}</div>
                                <!-- Con {{ }} hacemos interpolación -->
                                <!-- Con | indicamos transformaciones con un Pipe -->
                                <small>{{ receta.ingredientes | Ingredientes }}</small>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    `,

    // Con el metadato 'providers' indicamos a Angular cómo tiene que
    // instanciar aquellos servicios que vamos a usar en este componente.
    providers: [
        HTTP_PROVIDERS,
        RecetasService
    ],

    // Con el metadato 'pipes' indicamos a Angular cómo tiene que
    // instanciar aquellos Pipes que vamos a usar en este componente.
    pipes: [
        IngredientesPipe
    ]
})
// Cuando queramos que una clase implemente una interfaz, lo indicamos con 'implements' en su definición.
export class MisRecetasComponent implements OnInit {

    // Array de entidades de tipo Receta.
    recetas : Receta[];

    // Contructor de la clase.
    // Hacemos la inyección de RecetasService.
    constructor(private _recetasService : RecetasService) { }

    // Hook OnInit del componente.
    // Obtenemos la colección de recetas.
    ngOnInit() {
        this._recetasService
            .obtenerRecetas()
            .subscribe(
                recetas => this.recetas = recetas,
                error => alert(error)
            );
    }
}
