
// Imports.
import { bootstrap } from "@angular/platform-browser-dynamic";
import { AppComponent } from "./app.component";

// Con la función 'bootstrap' hacemos el bootstrapping del componente
// indicado, que por lo general es el componente raíz de nuestra app.
bootstrap(AppComponent);
