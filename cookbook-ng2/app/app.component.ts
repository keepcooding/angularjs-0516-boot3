
// Imports.
import { Component } from "@angular/core";
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from "@angular/router-deprecated";
import { MisRecetasComponent } from "./mis-recetas.component";
import { NuevaRecetaComponent } from "./nueva-receta.component";

// Toda clase decorada con @Component se comporta como un componente.
@Component({

    // Con el metadato 'selector' definimos el elemnto HTML que usaremos para instanciar este componente.
    selector: "app",

    // Con el metadato 'template' / 'templateUrls' establecemos la vista asociada al componente.
    template: `
        <div class="container">
            <div class="row">
                <div class="twelve columns">
                    <header>
                        <h1>Cookbook</h1>
                        <h5>Mis recetas de cocina</h5>
                    </header>
                </div>
            </div>
            <div class="row">
                <div class="twelve columns">
                    <nav class="barra-navegacion">
                        <ul>
                            <li>
                                <!-- Con [] enlazamos datos con una directiva o un componente -->
                                <a [routerLink]="['MisRecetas']">Mis Recetas</a>
                            </li>
                            <li>
                                <!-- Con [] enlazamos datos con una directiva o un componente -->
                                <a [routerLink]="['NuevaReceta']">Nueva Receta</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <router-outlet></router-outlet>
        </div>
    `,

    // Con el metadato 'styles' definimos los estilos CSS que afectan únicamente a este componente.
    styles: [`
        .barra-navegacion ul {
            height: 42px;
            border-bottom: 1px solid #1EAEDB;
        }
        .barra-navegacion li {
            display: inline-block;
        }
        .barra-navegacion a {
            text-decoration: none;
            display: inline-block;
            padding-top: 8px;
            padding-left: 20px;
            padding-right: 20px;
            height: 33px;
        }
        .barra-navegacion a:first-child {
            border-left: 1px solid transparent;
            border-right: 1px solid transparent;
        }
        /* Angular establece la clase 'router-link-active' en aquel elemento
           que tenga asociada la directiva 'routerLink' cuando la configuración
           de ruta indicada en dicha directiva se esté navegando */
        .barra-navegacion a.router-link-active {
            border-top: 1px solid #1EAEDB;
            border-left: 1px solid #1EAEDB;
            border-right: 1px solid #1EAEDB;
            border-bottom: 1px solid #ffffff;
        }
        .barra-navegacion a:hover {
            cursor: pointer;
        }
    `],

    // Con el metadato 'providers' indicamos a Angular cómo tiene que
    // instanciar aquellos servicios que voy a usar en este componente.
    providers: [
        ROUTER_PROVIDERS
    ],

    // Con el metadato 'directives' indicamos a Angular cómo tiene que instanciar
    // aquellas directivas y componentnes que vamos a usar en este componente.
    directives: [
        ROUTER_DIRECTIVES
    ]
})

// Con el decorador @RouteConfig establecemos las configuraciones
// de ruta que va a gestionar este componente.
@RouteConfig([{

    // Nombre de la configuración de ruta para referirse a ella desde una vista.
    name: "MisRecetas",

    // Ruta asociada a esta configuración y que se navegará cuando se pulse el enlace correspondiente.
    path: "/mis-recetas",

    // Componente a renderizar en el routerOutlet definido en la vista asociada.
    component: MisRecetasComponent,

    // Con 'useAsDefault' indicamos que esta ruta debe navegarse por defecto al pintar este componente.
    useAsDefault: true
}, {
    // Nombre de la configuración de ruta para referirse a ella desde una vista.
    name: "NuevaReceta",

    // Ruta asociada a esta configuración y que se navegará cuando se pulse el enlace correspondiente.
    path: "/nueva-receta",

    // Componente a renderizar en el routerOutlet definido en la vista asociada.
    component: NuevaRecetaComponent
}])
export class AppComponent { }
