
// Imports.
import { Ingrediente } from "./ingrediente";

// Los tipos de nuestra app pueden definirse como una clase (class) o como una interfaz (interface). Al transpilar
// el código TypeScript a Javascript, una clase genera más código de la cuenta que no vamos a aprovechar, de ahí
// que elijamos definir nuestros tipos como interfaces.

// Tipo Receta.
export interface Receta {

    // Con ? indicamos que la propiedad es opcional.
    id? : number;
    nombre : string;
    ingredientes : Ingrediente[]
}
