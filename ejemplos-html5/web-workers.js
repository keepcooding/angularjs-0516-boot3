
function cuentraAtras(desde) {

    // Notificamos al hilo principal.
    postMessage(desde);

    // Comprobamos que la cuanta atrás no ha llegado a 0.
    if (desde > 0) {

        // Decrementamos el valor de la cuenta atrás.
        desde -= 1;

        // Esperamos un segundo y lanzamos de nuevo la función.
        setTimeout(function() { cuentraAtras(desde); }, 1000);
    }
}

// Iniciamos la cuenta atrás.
cuentraAtras(10);